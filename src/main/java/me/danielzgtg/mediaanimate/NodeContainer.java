package me.danielzgtg.mediaanimate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class NodeContainer implements Timelinable {
	private final Map<Node, NodeMeta> data = new HashMap<>();

	public final void addNode(final Node node) {
		Validate.notNull(node);

		node.setContainer(this);
		this.data.put(node, new NodeMeta());
	}

	public final void removeNode(final Node node) {
		Validate.notNull(node);
		Validate.require(node.getContainer() == this);

		final NodeMeta meta = this.data.get(node);

		Validate.require(meta.downstreamNodes.isEmpty());

		for (final Node upstreamNode : meta.upstreamNodes.keySet()) {
			final Map<Node, int[]> downstreamNodes = this.data.get(upstreamNode).downstreamNodes;

			Validate.require(downstreamNodes.containsKey(node));
			Validate.require(downstreamNodes.get(node)[0] == 1);
			downstreamNodes.remove(node);
		}

		this.data.remove(node);
		node.setContainer(null);
	}

	@Override
	public final void gotoFrame(final int frame) {
		for (final Node node : data.keySet()) {
			node.gotoFrame(frame);
		}

		for (final Map.Entry<Node, NodeMeta> entry : data.entrySet()) {
			this.makeReady(entry.getKey(), entry.getValue(), new LinkedList<>());
		}
		//todo
	}

	private final void makeReady(final Node node, final NodeMeta meta, final List<Node> cycleDetect) {
		if (node.isReady()) return;

		if (cycleDetect.contains(node)) {
			throw new IllegalStateException("Circular Node Links! " + cycleDetect);
		}
		cycleDetect.add(node);

		for (final Node upstreamNode : meta.upstreamNodes.keySet()) {
			this.makeReady(upstreamNode, data.get(upstreamNode), cycleDetect);
		}

		for (final Map.Entry<String, NodeLink> inputLink : meta.inputLinks.entrySet()) {
			final NodeLink nodeLink = inputLink.getValue();
			node.input(inputLink.getKey(), nodeLink.upstreamNode.output(nodeLink.outputName));
		}

		node.work();
	}

	public final void linkNodes(final Node upstreamNode, final String outputName,
	                      final Node downstreamNode, final String inputName) {
		final NodeMeta upstreamMeta;
		if (upstreamNode != null) {
			Validate.notNull(outputName);
			Validate.require(upstreamNode.hasOutputName(outputName));
			Validate.require(upstreamNode.getContainer() == this);
			upstreamMeta = this.data.get(upstreamNode);
			Validate.notNull(upstreamMeta);
		} else {
			upstreamMeta = null;
		}

		Validate.notNull(downstreamNode);
		Validate.notNull(inputName);
		Validate.require(downstreamNode.hasInputName(inputName), "unknown downstream input: " + inputName +
				" in " + downstreamNode);
		Validate.require(downstreamNode.getContainer() == this);
		final NodeMeta downstreamMeta = this.data.get(downstreamNode);
		Validate.notNull(downstreamMeta);

		if (upstreamNode != null) {
			{
				final Map<Node, int[]> downstreamNodes = upstreamMeta.downstreamNodes;
				int[] rc;
				if (downstreamNodes.containsKey(downstreamNode)) {
					rc = downstreamNodes.get(downstreamNode);
				} else {
					rc = new int[1];
					downstreamNodes.put(downstreamNode, rc);
				}

				rc[0]++;
			}

			{
				final Map<Node, int[]> upstreamNodes = downstreamMeta.upstreamNodes;
				int[] rc;
				if (upstreamNodes.containsKey(upstreamNode)) {
					rc = upstreamNodes.get(upstreamNode);
				} else {
					rc = new int[1];
					upstreamNodes.put(upstreamNode, rc);
				}

				rc[0]++;
			}
		}

		{
			final NodeLink previous = downstreamMeta.inputLinks.get(inputName);
			if (previous != null) {
				final Node previousUpstream = previous.upstreamNode;

				{
					final Map<Node, int[]> downstreamNodes = this.data.get(previousUpstream).downstreamNodes;
					int[] rc;
					if (downstreamNodes.containsKey(downstreamNode)) {
						rc = downstreamNodes.get(downstreamNode);
					} else {
						throw new IllegalStateException();
					}

					switch (rc[0]--) {
						case 0:
							throw new IllegalStateException();
						case 1:
							downstreamNodes.remove(downstreamNode);
							break;
					}
				}

				{
					final Map<Node, int[]> upstreamNodes = downstreamMeta.upstreamNodes;
					int[] rc;
					if (upstreamNodes.containsKey(previousUpstream)) {
						rc = upstreamNodes.get(previousUpstream);
					} else {
						throw new IllegalStateException();
					}

					switch (rc[0]--) {
						case 0:
							throw new IllegalStateException();
						case 1:
							upstreamNodes.remove(previousUpstream);
							break;
					}
				}
			}
		}

		if (upstreamNode != null) {
			downstreamMeta.inputLinks.put(inputName, new NodeLink(upstreamNode, outputName));
		} else {
			downstreamMeta.inputLinks.remove(inputName);
		}
	}

	private static final class NodeMeta {
		/*packaged*/ final Map<Node, int[]> upstreamNodes = new HashMap<>();
		/*packaged*/ final Map<Node, int[]> downstreamNodes = new HashMap<>();
		/*packaged*/ final Map<String, NodeLink> inputLinks = new HashMap<>();
	}

	private static final class NodeLink {
		/*packaged*/ final Node upstreamNode;
		/*packaged*/ final String outputName;

		/*packaged*/ NodeLink(final Node upstreamNode, final String outputName) {
			this.upstreamNode = upstreamNode;
			this.outputName = outputName;
		}
	}
}
