package me.danielzgtg.mediaanimate.image;

import me.danielzgtg.compsci11_sem2_2017.common.MathUtils;
import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public final class MediaImage implements Cloneable {
	private int x, y, w, h;
	private double rot;
	private BufferedImage backend;

	public MediaImage(final BufferedImage backend) {
		this.setBackend(backend);
		this.w = backend.getWidth();
		this.h = backend.getHeight();
	}

	public final BufferedImage getBackend() {
		return backend;
	}

	private final void setBackend(final BufferedImage backend) {
		Validate.notNull(backend);

		this.backend = backend;
	}

	public final int getX() {
		return x;
	}

	public final void setX(final int x) {
		this.x = x;
	}

	public final int getY() {
		return y;
	}

	public final void setY(final int y) {
		this.y = y;
	}

	public final int getW() {
		return w;
	}

	public final void setW(final int w) {
		this.w = w;
	}

	public final int getH() {
		return h;
	}

	public final void setH(final int h) {
		this.h = h;
	}

	public final double getRot() {
		return rot;
	}

	public final void setRot(final double rot) {
		//this.rot = MathUtils.normalizeAngleRad(rot);
		this.rot = rot;
	}

	public final BufferedImage flatten() {
		final BufferedImage result = new BufferedImage(this.w, this.h, BufferedImage.TYPE_INT_ARGB);

//		final AffineTransform at = AffineTransform.getRotateInstance(MathUtils.normalizeAngleRad(this.rot),
//				this.backend.getWidth() / 2.0D, this.backend.getHeight() / 2.0D);
//		final AffineTransformOp ato = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
//		final AffineTransform at = new AffineTransform();
//		at.translate(this.w / 2.0D, this.h / 2.0D);
//		at.rotate(this.rot);
//		at.translate(this.w / -2.0D, this.h / -2.0D);

		final Graphics2D g = result.createGraphics();
//		g.drawImage(ato.filter(GraphicsUtils.smoothScaleImage(this.backend, this.w, this.h), null),
//				this.x, this.y, null);
		final double hw = this.w / 2.0D;
		final double hh = this.h / 2.0D;
		g.translate(hw, hh);
		g.rotate(this.rot);
		g.translate(-hw, -hh);
		g.drawImage(GraphicsUtils.smoothScaleImage(this.backend, this.w, this.h), 0, 0, null);
		g.dispose();

		return result;
	}

	@Override
	public MediaImage clone() {
		final BufferedImage newImg = new BufferedImage(
				this.backend.getWidth(), this.backend.getHeight(), BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g = newImg.createGraphics();
		g.drawImage(this.backend, 0, 0, null);
		g.dispose();
		final MediaImage result = new MediaImage(newImg);

		result.x = this.x;
		result.y = this.y;
		result.w = this.w;
		result.h = this.h;
		result.rot = this.rot;

		return result;
	}
}
