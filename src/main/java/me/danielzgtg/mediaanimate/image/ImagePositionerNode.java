package me.danielzgtg.mediaanimate.image;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ImagePositionerNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in");
		namesIn.add("xpos");
		namesIn.add("ypos");
		namesIn.add("rot");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public ImagePositionerNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private ImagePositionerNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object inObj = this.workParam("in");

		if (!(inObj instanceof MediaImage)) {
			throw new IllegalArgumentException();
		}

		final MediaImage in = (MediaImage) inObj;
		final MediaImage out = in.clone();

		final Object xpos = this.workParam("xpos");
		final Object ypos = this.workParam("ypos");
		final int xsize = out.getW();
		final int ysize = out.getH();
		final Object rot = this.workParam("rot");

		if (xpos instanceof Number) {
			out.setX(((Number) xpos).intValue() - (xsize / 2));
		}
		if (ypos instanceof Number) {
			out.setY(((Number) ypos).intValue() - (ysize / 2));
		}
		if (rot instanceof Number) {
			out.setRot(out.getRot() + ((Number) rot).doubleValue());
		}

		this.workResult("out", out);
	}
}
