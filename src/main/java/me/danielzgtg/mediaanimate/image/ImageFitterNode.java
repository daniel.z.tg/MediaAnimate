package me.danielzgtg.mediaanimate.image;

import me.danielzgtg.mediaanimate.Node;
import me.danielzgtg.mediaanimate.audio.MediaAudioSample;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ImageFitterNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in");
		namesIn.add("xpos");
		namesIn.add("ypos");
		namesIn.add("xsize");
		namesIn.add("ysize");
		namesIn.add("rot");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public ImageFitterNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private ImageFitterNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object inObj = this.workParam("in");

		if (!(inObj instanceof MediaImage)) {
			throw new IllegalArgumentException();
		}

		final MediaImage in = (MediaImage) inObj;
		final MediaImage out = in.clone();

		final Object xpos = this.workParam("xpos");
		final Object ypos = this.workParam("ypos");
		final Object xsize = this.workParam("xsize");
		final Object ysize = this.workParam("ysize");
		final Object rot = this.workParam("rot");

		if (xpos instanceof Number) {
			out.setX(out.getX() + ((Number) xpos).intValue());
		}
		if (ypos instanceof Number) {
			out.setY(out.getY() + ((Number) ypos).intValue());
		}
		if (xsize instanceof Number) {
			out.setW(((Number) xsize).intValue());
		}
		if (ysize instanceof Number) {
			out.setH(((Number) ysize).intValue());
		}
		if (rot instanceof Number) {
			out.setRot(out.getRot() + ((Number) rot).doubleValue());
		}

		this.workResult("out", out);
	}
}
