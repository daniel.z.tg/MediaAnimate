package me.danielzgtg.mediaanimate.image;

import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;
import me.danielzgtg.mediaanimate.Node;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ImageCompositionNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("bg");
		namesIn.add("alpha");
		namesIn.add("fg");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public ImageCompositionNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private ImageCompositionNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object bgObj = this.workParam("bg");
		final Object fgObj = this.workParam("fg");

		if (!(bgObj instanceof MediaImage)) {
			throw new IllegalArgumentException();
		}

		if (!(fgObj instanceof MediaImage)) {
			this.workResult("out", bgObj);
			return;
		}

		final MediaImage bg = (MediaImage) bgObj;
		final MediaImage fg = (MediaImage) fgObj;
		final MediaImage out = bg.clone();

		final float alpha;
		final Object alphaObj = this.workParam("alpha");

		if (alphaObj instanceof Number) {
			alpha = ((Number) alphaObj).floatValue();
		} else {
			alpha = 1.0F;
		}

		final BufferedImage backend = out.getBackend();
		final Graphics2D g = backend.createGraphics();
		final int w = fg.getW();
		final int h = fg.getH();
		final double hw = w / 2.0D;
		final double hh = h / 2.0D;
		g.translate(fg.getX() + hw, fg.getY() + hh);
		g.rotate(fg.getRot());
		g.translate(-hw, -hh);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		g.drawImage(GraphicsUtils.smoothScaleImage(fg.getBackend(), w, h), 0, 0, null);
		g.dispose();

		this.workResult("out", out);
	}
}
