package me.danielzgtg.mediaanimate.image;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.mediaanimate.Node;
import me.danielzgtg.mediaanimate.audio.MediaAudioSample;

import java.util.Collections;
import java.util.Set;

public final class ImageNode extends Node {

	private final MediaImage image;

	public ImageNode(final MediaImage image) {
		Validate.notNull(image);
		this.image = image;
	}

	private ImageNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		this.workResult("out", this.image);
	}
}
