package me.danielzgtg.mediaanimate.control;

import me.danielzgtg.mediaanimate.Node;

import java.util.Set;

public class FrameCounterNode extends Node {

	public FrameCounterNode() {}

	private FrameCounterNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {
		this.workResult("out", frame);
	}

	@Override
	protected void nodeWork() {}
}
