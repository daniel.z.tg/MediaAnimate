package me.danielzgtg.mediaanimate.control;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ChooseNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in1");
		namesIn.add("in2");
		namesIn.add("control1");
		namesIn.add("control2");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public ChooseNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private ChooseNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@SuppressWarnings("unchecked")
	@Override
	protected void nodeWork() {
		final Comparable control1 = (Comparable) this.workParam("control1");
		this.workResult("out", this.workParam(control1 == null || control1
				.compareTo(this.workParam("control2")) < 0 ? "in2" : "in1"));
	}
}
