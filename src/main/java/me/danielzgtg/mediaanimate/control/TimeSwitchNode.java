package me.danielzgtg.mediaanimate.control;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class TimeSwitchNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	private final int switchTime;
	private boolean choose2;

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in1");
		namesIn.add("in2");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public TimeSwitchNode(final int switchTime) {
		super(NAMES_IN, NAMES_OUT);

		this.switchTime = switchTime;
	}

	private TimeSwitchNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {
		this.choose2 = frame >= this.switchTime;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void nodeWork() {
		this.workResult("out", this.workParam(this.choose2 ? "in2" : "in1"));
	}
}
