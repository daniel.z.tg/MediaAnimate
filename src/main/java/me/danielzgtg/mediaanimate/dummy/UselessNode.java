package me.danielzgtg.mediaanimate.dummy;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.Set;

public final class UselessNode extends Node {
	public UselessNode() {
		super(Collections.emptySet(), Collections.emptySet());
	}

	private UselessNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {}
}
