package me.danielzgtg.mediaanimate.dummy;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.Set;

public final class PassthroughNode extends Node {
	private static final Set<String> NAMES_IN = Collections.singleton("in");
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	public PassthroughNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private PassthroughNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		this.workResult("out", this.workParam("in"));
	}
}
