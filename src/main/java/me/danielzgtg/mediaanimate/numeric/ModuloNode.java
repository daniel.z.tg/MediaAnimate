package me.danielzgtg.mediaanimate.numeric;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class ModuloNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final HashSet<String> tmpIn = new HashSet<>();

		tmpIn.add("arg1");
		tmpIn.add("arg2");

		NAMES_IN = Collections.unmodifiableSet(tmpIn);
	}

	public ModuloNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private ModuloNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object arg1 = this.workParam("arg1");
		final Object arg2 = this.workParam("arg2");

		final boolean fp = arg1 instanceof Double || arg2 instanceof Double;
		if (arg1 == null) {
			if (arg2 == null) {
				this.workResult("out", 0);
				return;
			}

			if (!(arg2 instanceof Number)) {
				throw new IllegalArgumentException();
			}

			if (fp) {
				this.workResult("out", 0);
			} else {
				this.workResult("out", 0);
			}
		} else {
			if (!(arg1 instanceof Number)) {
				throw new IllegalArgumentException();
			}

			if (arg2 == null) {
				if (fp) {
					this.workResult("out", ((Number) arg1).doubleValue() % 1);
				} else {
					this.workResult("out", 0);
				}

				return;
			}

			if (!(arg2 instanceof Number)) {
				throw new IllegalArgumentException();
			}

			if (fp) {
				this.workResult("out", ((Number) arg1).doubleValue() % ((Number) arg2).doubleValue());
			} else {
				this.workResult("out", ((Number) arg1).intValue() % ((Number) arg2).intValue());
			}
		}
	}
}
