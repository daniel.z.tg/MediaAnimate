package me.danielzgtg.mediaanimate.numeric;

import me.danielzgtg.mediaanimate.Node;

import java.util.Set;

public final class ConstantDoubleNode extends Node {
	private final Double output;

	public ConstantDoubleNode(final double output) {
		this.output = output;
	}

	private ConstantDoubleNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {
		this.workResult("out", this.output);
	}

	@Override
	protected void nodeWork() {}
}
