package me.danielzgtg.mediaanimate.numeric;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.Set;

public final class SineNode extends Node {
	private static final Set<String> NAMES_IN = Collections.singleton("in");
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	public SineNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private SineNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object in = this.workParam("in");

		if (in == null) {
			return;
		}

		if (!(in instanceof Number)) {
			throw new IllegalArgumentException();
		}

		this.workResult("out", Math.sin(((Number) in).doubleValue()));
	}
}
