package me.danielzgtg.mediaanimate.numeric;

import me.danielzgtg.mediaanimate.Node;

import java.util.Set;

public final class ConstantIntegerNode extends Node {
	private final Integer output;

	public ConstantIntegerNode(final int output) {
		this.output = output;
	}

	private ConstantIntegerNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {
		this.workResult("out", this.output);
	}

	@Override
	protected void nodeWork() {}
}
