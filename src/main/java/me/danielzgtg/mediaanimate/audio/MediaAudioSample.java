package me.danielzgtg.mediaanimate.audio;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public final class MediaAudioSample {
	private final /*mut*/ float[] data;
	//private byte[] data0;
	private final AudioFormat format;

	public static final AudioFormat CORE_FORMAT = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
			44100, 16, 1, 2, 44100, false);

	public final float[] getData() {
		return data;
	}

	public final AudioFormat getFormat() {
		return format;
	}

	public MediaAudioSample(final int samples) {
		this.data = new float[samples];
		this.format = CORE_FORMAT;
	}

	private MediaAudioSample(final byte[] data, final AudioFormat format) {
		Validate.notNull(data);
		Validate.require(CORE_FORMAT.matches(format));
//		Validate.notNull(format);
		final int length = data.length;
		Validate.require(length % 2 == 0);

		final float[] data2 = new float[length / 2];

		for (int i = 0; i < data2.length;) {
			final int i2 = i * 2;
			final byte b1 = data[i2];
			final byte b2 = data[i2 + 1];
			final short v1 = (short) ((short) (b1 & 0xFF) | (short) (b2 << 8));
			final float v = v1 / 32767.0F;
			if (v < -1 || v > 1) {
				throw new IllegalArgumentException();
			}
			data2[i++] = v;
//			{
//				final short f1 = (short) Math.round(v * 32767);
//				final byte b3 = (byte) (f1 & 0xFF);
//				final byte b4 = (byte) ((f1 & 0xFF00) >> 8);
//				if (b2 != b4) {
//					throw new RuntimeException();
//				} else if (b1 != b3) {
//					throw new RuntimeException();
//				}
//			}
		}

		this.data = data2;
		//this.data0 = data;
		this.format = format;
	}

	public static MediaAudioSample[] wrap(final AudioInputStream ais, final int fps) throws IOException {
		Validate.notNull(ais);

		final AudioFormat f = ais.getFormat();
		Validate.require(CORE_FORMAT.matches(f));
		//Validate.require(AudioFormat.Encoding.PCM_SIGNED.equals(f.getEncoding()));
		final int hz = Math.round(f.getFrameRate());
		Validate.require(f.getSampleRate() == hz);
		final int fs = f.getFrameSize();
		Validate.require(fs == 2);
		final int dataSize = fs * hz / fps;

		final MediaAudioSample[] result =
				new MediaAudioSample[(int) Math.ceil(ais.getFrameLength() / (double) hz * fps)];
		System.out.println("frames: " + result.length + ", seconds: " + (result.length / (double) fps)); // frames

		//boolean padded = false;
		for (int i = 0;; i++) {
			final byte[] buffer = new byte[dataSize];
			final int read = ais.read(buffer);
			if (read == -1) {
				Validate.require(i == result.length);
				break;
			} else if (read != dataSize) {
				for (int p = read; p < dataSize; p++) {
					buffer[p] = 0;
				}
				// Padded!
				//padded = true;
			}
			result[i] = new MediaAudioSample(buffer, f);
		}

		return result;
	}

	public static AudioInputStream unwrap(final MediaAudioSample[] samples) {
		Validate.notNull(samples);
		Validate.require(samples.length > 0);

		MediaAudioSample sample = samples[0];
		Validate.notNull(sample);
		final AudioFormat format = sample.format;
		int length = sample.data.length;
		for (int i = 1; i < samples.length; i++) {
			sample = samples[i];
			Validate.notNull(sample);
			if (format != sample.format) {
				throw new IllegalArgumentException();
			}
			length += sample.data.length;
		}

		int j = 0;
		final byte[] data = new byte[length * 2];
		for (int i = 0; i < samples.length; i++) {
			sample = samples[i];
			final float[] sampleData = sample.data;
			for (int k = 0; k < sampleData.length; k++) {
				final float f = sampleData[k];
				if (f < -1 || f > 1) {
					throw new IllegalArgumentException();
				}
				final short f1 = (short) Math.round(f * 32767);
				final byte b1 = (byte) (f1 & 0xFF);
				data[j++] = b1;
				final byte b2 = (byte) ((f1 & 0xFF00) >> 8);
				data[j++] = b2;
//				final int k2 = k * 2;
//				if (b2 != sample.data0[k2 + 1]) {
//					throw new RuntimeException();
//				} else if (b1 != sample.data0[k2]) {
//					throw new RuntimeException();
//				}
			}
		}
		Validate.require(j == data.length);

		return new AudioInputStream(new ByteArrayInputStream(data), format, j / 2);
	}
}
