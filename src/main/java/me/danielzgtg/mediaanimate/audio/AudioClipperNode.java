package me.danielzgtg.mediaanimate.audio;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class AudioClipperNode extends Node {
	private static final Set<String> NAMES_IN = Collections.singleton("in");
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	public AudioClipperNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private AudioClipperNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object inObj = this.workParam("in");

		if (!(inObj instanceof MediaAudioSample)) {
			throw new IllegalArgumentException();
		}

		final MediaAudioSample in = (MediaAudioSample) inObj;
		final float[] inData = in.getData();
		final int length = inData.length;
		final MediaAudioSample out = new MediaAudioSample(length);
		final float[] outData = out.getData();

		for (int i = 0; i < length; i++) {
			outData[i] = Math.min(1, Math.max(-1, inData[i]));
		}

		this.workResult("out", out);
	}
}
