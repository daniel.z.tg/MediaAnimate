package me.danielzgtg.mediaanimate.audio;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class AudioMixerNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in1");
		namesIn.add("in2");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public AudioMixerNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private AudioMixerNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object in1Obj = this.workParam("in1");
		final Object in2Obj = this.workParam("in2");

		if (!(in1Obj instanceof MediaAudioSample && in2Obj instanceof MediaAudioSample)) {
			throw new IllegalArgumentException();
		}

		final MediaAudioSample in1 = (MediaAudioSample) in1Obj;
		final MediaAudioSample in2 = (MediaAudioSample) in2Obj;

		final float[] in1Data = in1.getData();
		final float[] in2Data = in2.getData();
		final int length = in1Data.length;
		final MediaAudioSample out = new MediaAudioSample(length);
		final float[] outData = out.getData();
		Validate.require(length == in2Data.length);

		for (int i = 0; i < length; i++) {
			outData[i] = in1Data[i] + in2Data[i];
		}

		this.workResult("out", out);
	}
}
