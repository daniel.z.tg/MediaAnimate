package me.danielzgtg.mediaanimate.audio;

import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.Set;

public final class AudioNode extends Node {
	private static final Set<String> NAMES_IN = Collections.singleton("frame");
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	private final MediaAudioSample samples[];

	public AudioNode(final MediaAudioSample[] samples) {
		super(NAMES_IN, NAMES_OUT);
		this.samples = samples;
	}

	private AudioNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object frameObj = this.workParam("frame");

		if (!(frameObj instanceof Number)) {
			throw new IllegalArgumentException();
		}

		int frame;
		if (frameObj instanceof Double) {
			frame = (int) Math.round(((Double) frameObj));
		} else {
			frame = ((Number) frameObj).intValue();
		}

		this.workResult("out", this.samples[frame]);
	}
}
