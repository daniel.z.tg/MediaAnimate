package me.danielzgtg.mediaanimate.audio;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.mediaanimate.Node;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class AudioTunerNode extends Node {
	private static final Set<String> NAMES_IN;
	private static final Set<String> NAMES_OUT = Collections.singleton("out");

	static {
		final Set<String> namesIn = new HashSet<>();

		namesIn.add("in");
		namesIn.add("volume");

		NAMES_IN = Collections.unmodifiableSet(namesIn);
	}

	public AudioTunerNode() {
		super(NAMES_IN, NAMES_OUT);
	}

	private AudioTunerNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {}

	@Override
	protected void nodeWork() {
		final Object inObj = this.workParam("in");
		final Object volumeObj = this.workParam("volume");

		if (!(inObj instanceof MediaAudioSample)) {
			throw new IllegalArgumentException();
		}

		if (!(volumeObj instanceof Double)) {
			throw new IllegalArgumentException();
		}

		final MediaAudioSample in = (MediaAudioSample) inObj;
		final double volume = (Double) volumeObj;

		final float[] inData = in.getData();
		final int length = inData.length;
		final MediaAudioSample out = new MediaAudioSample(length);
		final float[] outData = out.getData();

		for (int i = 0; i < length; i++) {
			outData[i] = (float) (inData[i] * volume);
		}

		this.workResult("out", out);
	}
}
