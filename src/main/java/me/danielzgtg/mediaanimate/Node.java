package me.danielzgtg.mediaanimate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;

import java.util.*;

public abstract class Node implements Timelinable {
	private static final Set<String> DEFAULT_OUT = Collections.singleton("out");
	private Map<String, Integer> inputNames = new HashMap<>();
	private Map<String, Integer> outputNames = new HashMap<>();
	private final Object[] inputs;
	private final Object[] outputs;
	private boolean ready;
	private NodeContainer container;

	protected Node() {
		this(Collections.emptySet(), DEFAULT_OUT);
	}

	protected Node(final Set<String> inputNames, final Set<String> outputNames) {
		Validate.notNull(inputNames);
		Validate.notNull(outputNames);

		{
			int i = 0;
			for (final String input : inputNames) {
				this.inputNames.put(input, i++);
			}
			inputs = new Object[i];
		}

		{
			int i = 0;
			for (final String output : outputNames) {
				this.outputNames.put(output, i++);
			}
			outputs = new Object[i];
		}
	}

	public final Set<String> getInputNames() {
		return Collections.unmodifiableSet(new HashSet<>(inputNames.keySet()));
	}

	public final boolean hasInputName(final String name) {
		return this.inputNames.containsKey(name);
	}

	public final void input(final String name, final Object data) {
		Validate.notNull(name);
		Validate.require(!this.ready);
		Validate.notNull(this.container);

		final Integer lookup = inputNames.get(name);
		Validate.notNull(lookup);

		inputs[lookup] = data;
	}

	public final Set<String> getOutputNames() {
		return Collections.unmodifiableSet(new HashSet<>(outputNames.keySet()));
	}

	public final boolean hasOutputName(final String name) {
		return this.outputNames.containsKey(name);
	}

	public final Object output(final String name) {
		Validate.notNull(name);
		Validate.require(this.ready);

		final Integer lookup = outputNames.get(name);
		Validate.notNull(lookup);

		return outputs[lookup];
	}

	protected abstract void nodeGotoFrame(final int frame);

	@Override
	public final void gotoFrame(final int frame) {
		Validate.notNull(this.container);

		{
			final int length = inputs.length;
			for (int i = 0; i < length; i++) {
				inputs[i] = null;
			}
		}

		{
			final int length = outputs.length;
			for (int i = 0; i < length; i++) {
				outputs[i] = null;
			}
		}

		this.ready = false;
		nodeGotoFrame(frame);
	}

	public final void work() {
		Validate.require(!this.ready);
		Validate.notNull(this.container);

		nodeWork();
		this.ready = true;
	}

	protected abstract void nodeWork();

	protected final Object workParam(final String name) {
		Validate.notNull(name);
		Validate.require(!this.ready);

		final Integer lookup = this.inputNames.get(name);
		Validate.notNull(lookup);

		return this.inputs[lookup];
	}

	protected final void workResult(final String name, final Object result) {
		Validate.notNull(name);
		Validate.require(!this.ready);

		final Integer lookup = this.outputNames.get(name);
		Validate.notNull(lookup);

		this.outputs[lookup] = result;
	}

	public final boolean isReady() {
		return this.ready;
	}

	public final NodeContainer getContainer() {
		return this.container;
	}

	/*packaged*/ synchronized final void setContainer(final NodeContainer container) {
		if (container != null) {
			Validate.require(this.container == null);
		}

		this.container = container;
	}

	@Override
	public final int hashCode() {
		return super.hashCode();
	}

	@Override
	public final boolean equals(final Object other) {
		return this == other;
	}
}
