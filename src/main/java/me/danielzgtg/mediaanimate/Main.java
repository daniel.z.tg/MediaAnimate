package me.danielzgtg.mediaanimate;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.compsci11_sem2_2017.common.datastructure.Pair;
import me.danielzgtg.compsci11_sem2_2017.common.platform.ResourceUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.GraphicsUtils;
import me.danielzgtg.mediaanimate.control.ChooseNode;
import me.danielzgtg.mediaanimate.control.FrameCounterNode;
import me.danielzgtg.mediaanimate.control.TimeSwitchNode;
import me.danielzgtg.mediaanimate.dummy.PassthroughNode;
import me.danielzgtg.mediaanimate.dummy.UselessNode;
import me.danielzgtg.mediaanimate.easing.LinearEasingNode;
import me.danielzgtg.mediaanimate.easing.QuarticInEasingNode;
import me.danielzgtg.mediaanimate.easing.QuarticInOutEasingNode;
import me.danielzgtg.mediaanimate.easing.QuarticOutEasingNode;
import me.danielzgtg.mediaanimate.easing.QuarticOutInEasingNode;
import me.danielzgtg.mediaanimate.image.ImageCompositionNode;
import me.danielzgtg.mediaanimate.image.ImageFitterNode;
import me.danielzgtg.mediaanimate.image.ImageNode;
import me.danielzgtg.mediaanimate.image.ImagePositionerNode;
import me.danielzgtg.mediaanimate.image.MediaImage;
import me.danielzgtg.mediaanimate.numeric.AdditionNode;
import me.danielzgtg.mediaanimate.numeric.ConstantDoubleNode;
import me.danielzgtg.mediaanimate.numeric.ConstantIntegerNode;
import me.danielzgtg.mediaanimate.numeric.CosineNode;
import me.danielzgtg.mediaanimate.numeric.DegreesNode;
import me.danielzgtg.mediaanimate.numeric.DivisionNode;
import me.danielzgtg.mediaanimate.numeric.ModuloNode;
import me.danielzgtg.mediaanimate.numeric.MultiplicationNode;
import me.danielzgtg.mediaanimate.numeric.RadiansNode;
import me.danielzgtg.mediaanimate.numeric.RatioInverseNode;
import me.danielzgtg.mediaanimate.numeric.ReciprocalNode;
import me.danielzgtg.mediaanimate.numeric.SineNode;
import me.danielzgtg.mediaanimate.numeric.SubtractionNode;
import me.danielzgtg.mediaanimate.numeric.TangentNode;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class Main {

	private static final Map<String, BiFunction<Map<?, ?>, Map<String, Object>, Object>> LITERAL_SETUP_MAP;
	private static final Map<String, BiFunction<Map<?, ?>,
			Pair<Map<String, Object>, Map<String, Object>>, Node>> NODE_SETUP_MAP;
	private static final Map<String, BiFunction<Map<?, ?>, InputStream, Object>> RESOURCE_SETUP_MAP;
	private static final Map<String, BiFunction<Map<?, ?>,
			Pair<String, Pair<Node, String>>, Consumer<int[]>>> OUTPUT_SETUP_MAP;

	private static final Pattern ALLOWED_FILE_PATTERN = Pattern.compile("[.a-zA-Z0-9_]*");
	private static final Pattern ALLOWED_MASK_PATTERN = Pattern.compile("%0[1-9]+d");

	static {
		final Map<String, BiFunction<Map<?, ?>, Map<String, Object>, Object>> literalSetupMap = new HashMap<>();

		literalSetupMap.put("integerliteral", (map, literals) -> {
			final int num;
			{
				final Object tryNum = map.get("value");
				Validate.isType(tryNum, Number.class);
				num = ((Number) tryNum).intValue();
			}
			return num;
		});

		literalSetupMap.put("floatliteral", (map, literals) -> {
			final double num;
			{
				final Object tryNum = map.get("value");
				Validate.isType(tryNum, Number.class);
				num = ((Number) tryNum).doubleValue();
			}
			return num;
		});

		literalSetupMap.put("integercopy", (map, literals) -> {
			final String ref;
			{
				final Object tryRef = map.get("ref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final int num;
			{
				final Object tryNum = literals.get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).intValue();
			}
			return num;
		});

		literalSetupMap.put("floatcopy", (map, literals) -> {
			final String ref;
			{
				final Object tryRef = map.get("ref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final double num;
			{
				final Object tryNum = literals.get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).doubleValue();
			}
			return num;
		});

		literalSetupMap.put("integerbifunction", (map, literals) -> {
			final String func;
			{
				final Object tryRef = map.get("func");
				Validate.isType(tryRef, String.class);
				func = (String) tryRef;
			}

			final String ref1;
			{
				final Object tryRef = map.get("num1ref");
				Validate.isType(tryRef, String.class);
				ref1 = (String) tryRef;
			}

			final String ref2;
			{
				final Object tryRef = map.get("num2ref");
				Validate.isType(tryRef, String.class);
				ref2 = (String) tryRef;
			}

			final int num1;
			{
				final Object tryNum = literals.get(ref1);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref1);
				num1 = ((Number) tryNum).intValue();
			}

			final int num2;
			{
				final Object tryNum = literals.get(ref2);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref2);
				num2 = ((Number) tryNum).intValue();
			}

			if ("+".equals(func)) {
				return num1 + num2;
			} else if ("-".equals(func)) {
				return num1 - num2;
			} else if ("*".equals(func)) {
				return num1 * num2;
			} else if ("/".equals(func)) {
				return num1 / num2;
			} else if ("%".equals(func)) {
				return num1 % num2;
			} else {
				throw new RuntimeException("unknown integer bifunc: " + func);
			}
		});

		literalSetupMap.put("floatbifunction", (map, literals) -> {
			final String func;
			{
				final Object tryRef = map.get("func");
				Validate.isType(tryRef, String.class);
				func = (String) tryRef;
			}

			final String ref1;
			{
				final Object tryRef = map.get("num1ref");
				Validate.isType(tryRef, String.class);
				ref1 = (String) tryRef;
			}

			final String ref2;
			{
				final Object tryRef = map.get("num2ref");
				Validate.isType(tryRef, String.class);
				ref2 = (String) tryRef;
			}

			final double num1;
			{
				final Object tryNum = literals.get(ref1);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref1);
				num1 = ((Number) tryNum).doubleValue();
			}

			final double num2;
			{
				final Object tryNum = literals.get(ref2);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref2);
				num2 = ((Number) tryNum).doubleValue();
			}

			if ("+".equals(func)) {
				return num1 + num2;
			} else if ("-".equals(func)) {
				return num1 - num2;
			} else if ("*".equals(func)) {
				return num1 * num2;
			} else if ("/".equals(func)) {
				return num1 / num2;
			} else if ("%".equals(func)) {
				return num1 % num2;
			} else {
				throw new RuntimeException("unknown float bifunc: " + func);
			}
		});

		literalSetupMap.put("floatmonofunction", (map, literals) -> {
			final String func;
			{
				final Object tryRef = map.get("func");
				Validate.isType(tryRef, String.class);
				func = (String) tryRef;
			}

			final String ref;
			{
				final Object tryRef = map.get("numref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final double num;
			{
				final Object tryNum = literals.get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).doubleValue();
			}

			if ("()^-1".equals(func)) {
				return 1.0D / num;
			} else if ("1-()".equals(func)) {
				return 1.0D - num;
			} else if ("sin()".equals(func)) {
				return Math.sin(num);
			} else if ("cos()".equals(func)) {
				return Math.cos(num);
			} else if ("tan()".equals(func)) {
				return Math.tan(num);
			} else if ("degrees()".equals(func)) {
				return Math.toDegrees(num);
			} else if ("radians()".equals(func)) {
				return Math.toRadians(num);
			} else {
				throw new RuntimeException("unknown float monofunc: " + func);
			}
		});

		LITERAL_SETUP_MAP = Collections.unmodifiableMap(literalSetupMap);

		final Map<String, BiFunction<Map<?, ?>,
				Pair<Map<String, Object>, Map<String, Object>>, Node>> nodeSetupMap = new HashMap<>();

		nodeSetupMap.put("constantinteger", (map, literalsAndResources) -> {
			final String ref;
			{
				final Object tryRef = map.get("ref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final int num;
			{
				final Object tryNum = literalsAndResources.getLeft().get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).intValue();
			}

			return new ConstantIntegerNode(num);
		});

		nodeSetupMap.put("constantfloat", (map, literalsAndResources) -> {
			final String ref;
			{
				final Object tryRef = map.get("ref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final double num;
			{
				final Object tryNum = literalsAndResources.getLeft().get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).doubleValue();
			}

			return new ConstantDoubleNode(num);
		});

		nodeSetupMap.put("passthrough", (map, literalsAndResources) -> new PassthroughNode());

		nodeSetupMap.put("constantimage", (map, literalsAndResources) -> {
			final String ref;
			{
				final Object tryRef = map.get("res");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final MediaImage img;
			{
				final Object tryNum = literalsAndResources.getRight().get(ref);
				Validate.isType(tryNum, MediaImage.class, "unresolved image reference: " + ref);
				img = (MediaImage) tryNum;
			}

			return new ImageNode(img);
		});

		nodeSetupMap.put("imagefitter", (map, literalsAndResources) -> new ImageFitterNode());

		nodeSetupMap.put("imagepositioner", (map, literalsAndResources) -> new ImagePositionerNode());

		nodeSetupMap.put("imagecomposition", (map, literalsAndResources) -> new ImageCompositionNode());

		nodeSetupMap.put("easing", (map, literalsAndResources) -> {
			final Map<String, Object> literals = literalsAndResources.getLeft();

			final String func;
			{
				final Object tryFunc = map.get("func");
				Validate.isType(tryFunc, String.class);
				func = (String) tryFunc;
			}

			final String frameStartRef;
			{
				final Object tryRef = map.get("framestartref");
				Validate.isType(tryRef, String.class);
				frameStartRef = (String) tryRef;
			}

			final String frameEndRef;
			{
				final Object tryRef = map.get("frameendref");
				Validate.isType(tryRef, String.class);
				frameEndRef = (String) tryRef;
			}

			final String initialValueRef;
			{
				final Object tryRef = map.get("initialvalueref");
				Validate.isType(tryRef, String.class);
				initialValueRef = (String) tryRef;
			}

			final String finalValueRef;
			{
				final Object tryRef = map.get("finalvalueref");
				Validate.isType(tryRef, String.class);
				finalValueRef = (String) tryRef;
			}

			final int frameStart;
			{
				final Object tryNum = literals.get(frameStartRef);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + frameStartRef);
				frameStart = ((Number) tryNum).intValue();
			}

			final int frameEnd;
			{
				final Object tryNum = literals.get(frameEndRef);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + frameEndRef);
				frameEnd = ((Number) tryNum).intValue();
			}

			final double initialValue;
			{
				final Object tryNum = literals.get(initialValueRef);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + initialValueRef);
				initialValue = ((Number) tryNum).doubleValue();
			}

			final double finalValue;
			{
				final Object tryNum = literals.get(finalValueRef);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + finalValueRef);
				finalValue = ((Number) tryNum).doubleValue();
			}

			if ("linear".equals(func)) {
				return new LinearEasingNode(frameStart, frameEnd, initialValue, finalValue);
			} else if ("quarticout".equals(func)) {
				return new QuarticOutEasingNode(frameStart, frameEnd, initialValue, finalValue);
			} else if ("quarticin".equals(func)) {
				return new QuarticInEasingNode(frameStart, frameEnd, initialValue, finalValue);
			} else if ("quarticinout".equals(func)) {
				return new QuarticInOutEasingNode(frameStart, frameEnd, initialValue, finalValue);
			} else if ("quarticoutin".equals(func)) {
				return new QuarticOutInEasingNode(frameStart, frameEnd, initialValue, finalValue);
			} else {
				throw new RuntimeException("unknown easing func: " + func);
			}
		});

		nodeSetupMap.put("numericbifunction", (map, literalsAndResources) -> {
			final String func;
			{
				final Object tryRef = map.get("func");
				Validate.isType(tryRef, String.class);
				func = (String) tryRef;
			}

			if ("+".equals(func)) {
				return new AdditionNode();
			} else if ("-".equals(func)) {
				return new SubtractionNode();
			} else if ("*".equals(func)) {
				return new MultiplicationNode();
			} else if ("/".equals(func)) {
				return new DivisionNode();
			} else if ("%".equals(func)) {
				return new ModuloNode();
			} else {
				throw new RuntimeException("unknown bifunc: " + func);
			}
		});

		nodeSetupMap.put("numericmonofunction", (map, literalsAndResources) -> {
			final String func;
			{
				final Object tryRef = map.get("func");
				Validate.isType(tryRef, String.class);
				func = (String) tryRef;
			}

			if ("()^-1".equals(func)) {
				return new ReciprocalNode();
			} else if ("1-()".equals(func)) {
				return new RatioInverseNode();
			} else if ("sin()".equals(func)) {
				return new SineNode();
			} else if ("cos()".equals(func)) {
				return new CosineNode();
			} else if ("tan()".equals(func)) {
				return new TangentNode();
			} else if ("degrees()".equals(func)) {
				return new DegreesNode();
			} else if ("radians()".equals(func)) {
				return new RadiansNode();
			} else {
				throw new RuntimeException("unknown monofunc");
			}
		});

		nodeSetupMap.put("useless", (map, literalsAndResources) -> new UselessNode());

		nodeSetupMap.put("framecounter", (map, literalsAndResources) -> new FrameCounterNode());

		nodeSetupMap.put("choose", (map, literalsAndResources) -> new ChooseNode());

		nodeSetupMap.put("timeswitch", (map, literalsAndResources) -> {
			final String ref;
			{
				final Object tryRef = map.get("timeref");
				Validate.isType(tryRef, String.class);
				ref = (String) tryRef;
			}

			final int num;
			{
				final Object tryNum = literalsAndResources.getLeft().get(ref);
				Validate.isType(tryNum, Number.class, "unresolved numeric reference: " + ref);
				num = ((Number) tryNum).intValue();
			}

			return new TimeSwitchNode(num);
		});

		NODE_SETUP_MAP = Collections.unmodifiableMap(nodeSetupMap);

		final Map<String, BiFunction<Map<?, ?>, InputStream, Object>> resourceSetupMap = new HashMap<>();

		resourceSetupMap.put("image", (map, is) -> {
			final int width;
			{
				final Object tryWidth = map.get("width");
				Validate.isType(tryWidth, Number.class);
				width = ((Number) tryWidth).intValue();
			}

			final int height;
			{
				final Object tryHeight = map.get("height");
				Validate.isType(tryHeight, Number.class);
				height = ((Number) tryHeight).intValue();
			}

			try {
				return new MediaImage(GraphicsUtils.smoothScaleImage(ImageIO.read(is), width, height));
			} catch (final Throwable t) {
				throw new RuntimeException(t);
			}
		});

		RESOURCE_SETUP_MAP = Collections.unmodifiableMap(resourceSetupMap);

		final Map<String, BiFunction<Map<?, ?>, Pair<String, Pair<Node, String>>, Consumer<int[]>>> outputSetupMap
				= new HashMap<>();

		outputSetupMap.put("integer", (map, rootPair) -> {
			final String outputSpec = rootPair.getLeft();
			final Pair<Node, String> subPair = rootPair.getRight();
			final Node node = subPair.getLeft();
			final String outPort = subPair.getRight();

			return (num) -> {
				final Object out = node.output(outPort);
				Validate.isType(out, Number.class);
				final int result = ((Number) out).intValue();

				try {
				final File file = new File(String.format(outputSpec, num[0]));
					file.delete();
					file.createNewFile();
					try (PrintWriter target = new PrintWriter(file)) {
						target.println(result);
					}
				} catch (final Throwable t) {
					throw new RuntimeException(t);
				}
			};
		});

		outputSetupMap.put("float", (map, rootPair) -> {
			final String outputSpec = rootPair.getLeft();
			final Pair<Node, String> subPair = rootPair.getRight();
			final Node node = subPair.getLeft();
			final String outPort = subPair.getRight();

			return (num) -> {
				final Object out = node.output(outPort);
				Validate.isType(out, Number.class);
				final double result = ((Number) out).doubleValue();

				try {
					final File file = new File(String.format(outputSpec, num[0]));
					file.delete();
					file.createNewFile();
					try (PrintWriter target = new PrintWriter(file)) {
						target.println(result);
					}
				} catch (final Throwable t) {
					throw new RuntimeException(t);
				}
			};
		});

		outputSetupMap.put("image", (map, rootPair) -> {
			final String outputSpec = rootPair.getLeft();
			final Pair<Node, String> subPair = rootPair.getRight();
			final Node node = subPair.getLeft();
			final String outPort = subPair.getRight();

			return (num) -> {
				final Object out = node.output(outPort);
				Validate.isType(out, MediaImage.class);
				final BufferedImage result = ((MediaImage) out).flatten();

				try {
					final File file = new File(String.format(outputSpec, num[0]));
					file.delete();
					file.createNewFile();
					ImageIO.write(result, "png", file);
				} catch (final Throwable t) {
					throw new RuntimeException(t);
				}
			};
		});

		OUTPUT_SETUP_MAP = Collections.unmodifiableMap(outputSetupMap);
	}

	public static void main(String[] ignore) throws Throwable {
		final Map<?, ?> input = ResourceUtils.loadConstantsMap("mediaproj.cfg",
				ResourceUtils.EXTERNAL_RESOURCE_LOADER);
		run(input);
	}

	public static void run(final Map<?, ?> inputRaw) throws Throwable {
		Validate.notNull(inputRaw);

		final String inputDir = "input/";
		{
			final File inputDirFile = new File(inputDir);
			Validate.require(inputDirFile.exists() && inputDirFile.isDirectory());
		}

		final String outputDir = "output/";
		{
			final File outputDirFile = new File(outputDir);
			if (outputDirFile.exists()) {
				Validate.require(outputDirFile.isDirectory());
			} else {
				outputDirFile.mkdir();
				Validate.require(outputDirFile.exists() && outputDirFile.isDirectory());
			}
		}

		final Map<?, ?> input;
		{
			final Object tryInput = inputRaw.get("me.danielzgtg.MediaAnimate");
			Validate.isType(tryInput, Map.class);
			input = (Map<?, ?>) tryInput;
		}

		final Map<?, ?> env;
		{
			final Object tryEnv = input.get("environment");
			Validate.isType(tryEnv, Map.class);
			env = (Map<?, ?>) tryEnv;
		}

		final int fps;
		{
			final Object tryFps = env.get("framerate");
			Validate.isType(tryFps, Number.class);
			fps = ((Number) tryFps).intValue();
			Validate.require(fps >= 1);
		}

		final int duration;
		{
			final Object tryDuration = env.get("duration");
			Validate.isType(tryDuration, Number.class);
			duration = ((Number) tryDuration).intValue();
			Validate.require(duration >= 1);
		}

		final int batchSize;
		{
			final Object tryBatchSize = env.get("batchsize");
			Validate.isType(tryBatchSize, Number.class);
			batchSize = ((Number) tryBatchSize).intValue();
			Validate.require(batchSize >= 1);
		}

		final List<Map<?, ?>> literalsIn;
		{
			final Object tryLiterals = input.get("literals");
			Validate.isType(tryLiterals, List.class);
			final List<?> tryLiterals2 = (List<?>) tryLiterals;
			Validate.allAreType(tryLiterals2, Map.class);
			//noinspection unchecked
			literalsIn = (List<Map<?, ?>>) tryLiterals2;
		}

		final Map<String, Object> literals;
		{
			final Map<String, Object> literalsMut = new HashMap<>();
			literalsMut.put("pi", Math.PI);
			literalsMut.put("2pi", Math.PI * 2);
			literalsMut.put("e", Math.E);
			literalsMut.put("zero", 0);
			literalsMut.put("one", 1);
			literalsMut.put("two", 2);
			literalsMut.put("duration", duration);
			literalsMut.put("fps", fps);
			for (final Map<?, ?> literal : literalsIn) {
				final String id;
				{
					final Object tryId = literal.get("id");
					Validate.isType(tryId, String.class);
					id = (String) tryId;
				}
				Validate.require(!literalsMut.containsKey(id), "duplicate literal: " + id);

				final String type;
				{
					final Object tryType = literal.get("type");
					Validate.isType(tryType, String.class);
					type = (String) tryType;
				}

				final BiFunction<Map<?, ?>, Map<String, Object>, Object> literalDecoder = LITERAL_SETUP_MAP.get(type);
				Validate.notNull(literalDecoder, "unknown literal type: " + type);

				final Object value = literalDecoder.apply(literal, literalsMut);
				literalsMut.put(id, value);
			}
			literals = Collections.unmodifiableMap(literalsMut);
		}

		final List<Map<?, ?>> inputResIn;
		{
			final Object tryInputRes = input.get("inputresources");
			Validate.isType(tryInputRes, List.class);
			final List<?> tryInputRes2 = (List<?>) tryInputRes;
			Validate.allAreType(tryInputRes2, Map.class);
			//noinspection unchecked
			inputResIn = (List<Map<?, ?>>) tryInputRes2;
		}

		final Map<String, Object> inputRes = new HashMap<>();
		for (final Map resIn : inputResIn) {
			final String id;
			{
				final Object tryId = resIn.get("id");
				Validate.isType(tryId, String.class);
				id = (String) tryId;
			}
			Validate.require(!resIn.containsKey(id), "duplicate resource: " + id);

			final String fileName;
			{
				final Object tryId = resIn.get("file");
				Validate.isType(tryId, String.class);
				fileName = (String) tryId;
			}
			Validate.require(ALLOWED_FILE_PATTERN.matcher(fileName).matches());

			final String type;
			{
				final Object tryType = resIn.get("type");
				Validate.isType(tryType, String.class);
				type = (String) tryType;
			}

			final BiFunction<Map<?, ?>, InputStream, Object> resourceDecoder = RESOURCE_SETUP_MAP.get(type);
			Validate.notNull(resourceDecoder);

			inputRes.put(id, resourceDecoder.apply(resIn, new FileInputStream(new File(inputDir + fileName))));
		}

		final List<Map<?, ?>> outputFileSpecsIn;
		{
			final Object tryOutputFileSpecs = input.get("outputfilespecs");
			Validate.isType(tryOutputFileSpecs, List.class);
			final List<?> tryOutputFileSpecs2 = (List<?>) tryOutputFileSpecs;
			Validate.allAreType(tryOutputFileSpecs2, Map.class);
			//noinspection unchecked
			outputFileSpecsIn = (List<Map<?, ?>>) tryOutputFileSpecs2;
		}

		final Map<String, String> outputFileSpecs;
		{
			final Map<String, String> outputFileSpecsMut = new HashMap<>();
			for (Map<?, ?> spec : outputFileSpecsIn) {
				final String id;
				{
					final Object tryId = spec.get("id");
					Validate.isType(tryId, String.class);
					id = (String) tryId;
				}
				Validate.require(!outputFileSpecsMut.containsKey(id), "duplicated outputspec: " + id);

				final String prefix;
				{
					final Object tryPrefix = spec.get("prefix");
					Validate.isType(tryPrefix, String.class);
					prefix = (String) tryPrefix;
				}

				final String frameMask;
				{
					final Object tryFrameMask = spec.get("fileframemask");
					Validate.isType(tryFrameMask, String.class);
					frameMask = (String) tryFrameMask;
				}

				final String suffix;
				{
					final Object tryId = spec.get("suffix");
					Validate.isType(tryId, String.class);
					suffix = (String) tryId;
				}

				Validate.require(ALLOWED_FILE_PATTERN.matcher(prefix).matches());
				Validate.require(ALLOWED_FILE_PATTERN.matcher(suffix).matches());
				Validate.require(ALLOWED_MASK_PATTERN.matcher(frameMask).matches());

				outputFileSpecsMut.put(id, outputDir + prefix + frameMask + suffix);
			}
			outputFileSpecs = Collections.unmodifiableMap(outputFileSpecsMut);
		}

		final Map<?, ?> scene;
		{
			final Object tryScene = input.get("scene");
			Validate.isType(tryScene, Map.class);
			scene = (Map<?, ?>) tryScene;
		}

		final Stack<Pair<Integer, Integer>> batches = new Stack<>();
		{
			int frame = 0;
			while (true) {
				final int lastFrame = frame;
				frame += batchSize;
				if (frame > duration - 1) {
					frame = duration;
					batches.push(new Pair<>(lastFrame, frame));
					break;
				} else {
					batches.push(new Pair<>(lastFrame, frame));
				}
			}
		}

		final Object lock = new Object();
		final List<Thread> joinList = new LinkedList<>();
		final String threadPrefix = Thread.currentThread().getName();
		final Pair<Map<String, Object>, Map<String, Object>> literalsAndResources = new Pair<>(literals, inputRes);
		for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
			final int threadId = i;
			final Thread t = new Thread(() -> {
				System.out.println("[MediaAnimate] Worker Thread " + threadId + " starting");
				while (true) {
					final Pair<Integer, Integer> batch;

					synchronized (lock) {
						if (batches.isEmpty()) {
							break;
						}

						batch = batches.pop();
					}

					final int frameStart = batch.getLeft();
					final int frameEnd = batch.getRight();

					System.out.println("[MediaAnimate] Worker Thread " + threadId +
							": at batch [" +  frameStart + "," + frameEnd + "]");
					runScene(scene, literalsAndResources, outputFileSpecs, frameStart, frameEnd);
				}

				System.out.println("[MediaAnimate] Worker Thread " + threadId + " stopping");
			});
			t.setName(threadPrefix + ": MediaAnimate Worker " + threadId);
			joinList.add(t);
			t.start();
		}
		for (final Thread thread : joinList) {
			thread.join();
		}
	}

	private static void runScene(final Map<?, ?> scene,
	                             final Pair<Map<String, Object>, Map<String, Object>> literalsAndResources,
	                             final Map<String, String> outputFileSpecs, final int frameStart, final int frameEnd) {
		final List<Map<?, ?>> nodesIn;
		{
			final Object tryNodes = scene.get("nodes");
			Validate.isType(tryNodes, List.class);
			final List<?> tryNodes2 = (List<?>) tryNodes;
			Validate.allAreType(tryNodes2, Map.class);
			//noinspection unchecked
			nodesIn = (List<Map<?, ?>>) tryNodes2;
		}

		final Map<String, Node> nodes = new HashMap<>();
		for (final Map<?, ?> node : nodesIn) {
			final String id;
			{
				final Object tryId = node.get("id");
				Validate.isType(tryId, String.class);
				id = (String) tryId;
			}
			Validate.require(!nodes.containsKey(id), "duplicated node: " + id);

			final String type;
			{
				final Object tryType = node.get("type");
				Validate.isType(tryType, String.class);
				type = (String) tryType;
			}

			final BiFunction<Map<?, ?>, Pair<Map<String, Object>, Map<String, Object>>, Node>
					nodeDecoder = NODE_SETUP_MAP.get(type);
			Validate.notNull(nodeDecoder, "unknown node type: " + type);

			final Node value = nodeDecoder.apply(node, literalsAndResources);
			nodes.put(id, value);
		}

		final NodeContainer nc = new NodeContainer();
		nodes.values().forEach(nc::addNode);

		final List<Map<?, ?>> linksIn;
		{
			final Object tryLinks = scene.get("links");
			Validate.isType(tryLinks, List.class);
			final List<?> tryLinks2 = (List<?>) tryLinks;
			Validate.allAreType(tryLinks2, Map.class);
			//noinspection unchecked
			linksIn = (List<Map<?, ?>>) tryLinks2;
		}

		for (final Map<?, ?> link : linksIn) {
			final String outRef;
			{
				final Object tryOutRef = link.get("outref");
				Validate.isType(tryOutRef, String.class);
				outRef = (String) tryOutRef;
			}

			final Node outNode;
			{
				final Node tryOutNode = nodes.get(outRef);
				Validate.notNull(tryOutNode, "outnode not found: " + outRef);
				outNode = tryOutNode;
			}

			final String outPort;
			{
				final Object tryOutPort = link.get("outport");
				Validate.isType(tryOutPort, String.class);
				outPort = (String) tryOutPort;
			}

			final String inRef;
			{
				final Object tryInRef = link.get("inref");
				Validate.isType(tryInRef, String.class);
				inRef = (String) tryInRef;
			}

			final Node inNode;
			{
				final Node tryInNode = nodes.get(inRef);
				Validate.notNull(tryInNode, "innode not found: " + inRef);
				inNode = tryInNode;
			}

			final String inPort;
			{
				final Object tryInPort = link.get("inport");
				Validate.isType(tryInPort, String.class);
				inPort = (String) tryInPort;
			}

			nc.linkNodes(outNode, outPort, inNode, inPort);
		}

		final List<Map<?, ?>> outputsIn;
		{
			final Object tryOutputs = scene.get("outputs");
			Validate.isType(tryOutputs, List.class);
			final List<?> tryOutputs2 = (List<?>) tryOutputs;
			Validate.allAreType(tryOutputs2, Map.class);
			//noinspection unchecked
			outputsIn = (List<Map<?, ?>>) tryOutputs2;
		}

		final List<Consumer<int[]>> outputs;
		{
			final List<Consumer<int[]>> outputsMut = new LinkedList<>();
			final List<String> usedSpecs = new LinkedList<>();
			for (final Map<?, ?> output : outputsIn) {
				final String specRef;
				{
					final Object trySpecRef = output.get("filespecref");
					Validate.isType(trySpecRef, String.class);
					specRef = (String) trySpecRef;
				}
				Validate.require(!usedSpecs.contains(specRef));
				usedSpecs.add(specRef);

				final String spec = outputFileSpecs.get(specRef);
				Validate.notNull(spec);

				final String outRef;
				{
					final Object tryOutRef = output.get("outref");
					Validate.isType(tryOutRef, String.class);
					outRef = (String) tryOutRef;
				}

				final Node outNode;
				{
					final Node tryOutNode = nodes.get(outRef);
					Validate.notNull(tryOutNode);
					outNode = tryOutNode;
				}

				final String outPort;
				{
					final Object tryOutPort = output.get("outport");
					Validate.isType(tryOutPort, String.class);
					outPort = (String) tryOutPort;
				}

				final String type;
				{
					final Object tryType = output.get("type");
					Validate.isType(tryType, String.class);
					type = (String) tryType;
				}

				final BiFunction<Map<?, ?>, Pair<String, Pair<Node, String>>, Consumer<int[]>> outputLambdaGenerator
						= OUTPUT_SETUP_MAP.get(type);
				Validate.notNull(outputLambdaGenerator, "unknown output type: " + type);

				outputsMut.add(outputLambdaGenerator.apply(output, new Pair<>(spec, new Pair<>(outNode, outPort))));
			}

			outputs = Collections.unmodifiableList(outputsMut);
		}


		for (int[] i = { frameStart }; i[0] < frameEnd; i[0]++) {
			nc.gotoFrame(i[0]);
			for (Consumer<int[]> output : outputs) {
				output.accept(i);
			}
		}
	}

	private static void render(final int frameStart, final int frameEnd) throws Throwable {
		// Setup
		final MediaImage bg = new MediaImage(GraphicsUtils.smoothScaleImage(
				ImageIO.read(new File("input/risk.png")), 1920, 1080));
		final MediaImage fg1 = new MediaImage(GraphicsUtils.smoothScaleImage(
				ImageIO.read(new File("input/orangejuice.png")), 360, 540));
		final NodeContainer nc = new NodeContainer();
		final ImageNode bgNode = new ImageNode(bg);
		final LinearEasingNode fgT = new LinearEasingNode(0, 120, 0.0, 1.0);
		final ConstantIntegerNode fgY = new ConstantIntegerNode(270);
		final ImageNode fg1Node = new ImageNode(fg1);
		final ImageFitterNode fg1Fit = new ImageFitterNode();
		final QuarticOutEasingNode fg1X = new QuarticOutEasingNode(0, 120, -360, 300);
		final QuarticOutEasingNode fg1R = new QuarticOutEasingNode(0, 120, Math.PI * 2, 0.0);
		final ImageCompositionNode fg1Comp = new ImageCompositionNode();
		final ImageFitterNode fg2Fit = new ImageFitterNode();
		final QuarticOutEasingNode fg2X = new QuarticOutEasingNode(0, 120, 1920, 1260);
		final QuarticOutEasingNode fg2R = new QuarticOutEasingNode(0, 120, Math.PI * -2, 0.0);
		final ImageCompositionNode fg2Comp = new ImageCompositionNode();
		nc.addNode(fgT);
		nc.addNode(fgY);
		nc.addNode(bgNode);
		nc.addNode(fg1Node);
		nc.addNode(fg1Fit);
		nc.addNode(fg1X);
		nc.addNode(fg1R);
		nc.addNode(fg1Comp);
		nc.linkNodes(fg1Node, "out", fg1Fit, "in");
		nc.linkNodes(fg1X, "out", fg1Fit, "xpos");
		nc.linkNodes(fgY, "out", fg1Fit, "ypos");
		nc.linkNodes(fg1R, "out", fg1Fit, "rot");
		nc.linkNodes(fgT, "out", fg1Comp, "alpha");
		nc.linkNodes(bgNode, "out", fg1Comp, "bg");
		nc.linkNodes(fg1Fit, "out", fg1Comp, "fg");
		nc.addNode(fg2Fit);
		nc.addNode(fg2X);
		nc.addNode(fg2R);
		nc.addNode(fg2Comp);
		nc.linkNodes(fg1Node, "out", fg2Fit, "in");
		nc.linkNodes(fg2X, "out", fg2Fit, "xpos");
		nc.linkNodes(fgY, "out", fg2Fit, "ypos");
		nc.linkNodes(fg2R, "out", fg2Fit, "rot");
		nc.linkNodes(fgT, "out", fg2Comp, "alpha");
		nc.linkNodes(fg1Comp, "out", fg2Comp, "bg");
		nc.linkNodes(fg2Fit, "out", fg2Comp, "fg");

		// Render
		for (int i = frameStart; i < frameEnd; i++) {
			nc.gotoFrame(i);
			final MediaImage out = (MediaImage) fg2Comp.output("out");
			ImageIO.write(out.flatten(), "PNG", new File(String.format("output/frame%03d.png", i)));
		}
	}
}
