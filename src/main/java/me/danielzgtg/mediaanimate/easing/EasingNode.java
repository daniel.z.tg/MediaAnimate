package me.danielzgtg.mediaanimate.easing;

import me.danielzgtg.compsci11_sem2_2017.common.Validate;
import me.danielzgtg.mediaanimate.Node;

import java.util.Set;

public abstract class EasingNode extends Node {
	protected final int easeStart;
	protected final int easeEnd;
	protected final int duration;
	protected final double startValue;
	protected final double endValue;
	protected final double change;

	protected EasingNode(final int easeStart, final int easeEnd,
	                     final double startValue, final double endValue) {
		Validate.require(easeStart >= 0 && easeEnd >= 0 && easeStart < easeEnd);
		this.easeStart = easeStart;
		this.easeEnd = easeEnd;
		this.duration = easeEnd - easeStart;
		this.startValue = startValue;
		this.endValue = endValue;
		this.change = endValue - startValue;
	}

	private EasingNode(final Set<String> inputNames, final Set<String> outputNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void nodeGotoFrame(final int frame) {
		if (frame <= this.easeStart) {
			this.workResult("out", this.startValue);
		} else if (frame > this.easeEnd) {
			this.workResult("out", this.endValue);
		} else {
			this.workResult("out", this.ease((frame - this.easeStart) / (double) this.duration));
		}
	}

	protected abstract double ease(final double positionRatio);

	@Override
	protected void nodeWork() {}
}
