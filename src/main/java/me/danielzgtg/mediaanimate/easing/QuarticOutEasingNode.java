package me.danielzgtg.mediaanimate.easing;

public final class QuarticOutEasingNode extends EasingNode {
	public QuarticOutEasingNode(final int easeStart, final int easeEnd, final double startValue, final double endValue) {
		super(easeStart, easeEnd, startValue, endValue);
	}

	@Override
	protected double ease(final double p) {
		final double p2 = p - 1;
		return this.change * (1 - (p2 * p2 * p2 * p2)) + this.startValue;
	}
}
