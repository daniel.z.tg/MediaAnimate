package me.danielzgtg.mediaanimate.easing;

public final class QuarticInOutEasingNode extends EasingNode {
	public QuarticInOutEasingNode(final int easeStart, final int easeEnd, final double startValue, final double endValue) {
		super(easeStart, easeEnd, startValue, endValue);
	}

	@Override
	protected double ease(final double positionRatio) {
		final double base;
		if (positionRatio < 0.5) {
			base = 8 * positionRatio * positionRatio * positionRatio * positionRatio;
		} else {
			final double p = positionRatio - 1;
			base = 1 - (8 * p * p * p * p);
		}

		return this.change * base + this.startValue;
	}
}
