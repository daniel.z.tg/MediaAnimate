package me.danielzgtg.mediaanimate.easing;

public final class QuarticOutInEasingNode extends EasingNode {
	public QuarticOutInEasingNode(final int easeStart, final int easeEnd, final double startValue, final double endValue) {
		super(easeStart, easeEnd, startValue, endValue);
	}

	@Override
	protected double ease(final double positionRatio) {
		final double p = positionRatio - 0.5D;
		final double base;
		if (positionRatio > 0.5) {
			base = 8 * p * p * p * p + 0.5D;
		} else {
			base = 0.5D - (8 * p * p * p * p);
		}

		return this.change * base + this.startValue;
	}
}
