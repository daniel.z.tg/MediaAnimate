package me.danielzgtg.mediaanimate.easing;

public final class LinearEasingNode extends EasingNode {
	public LinearEasingNode(final int easeStart, final int easeEnd, final double startValue, final double endValue) {
		super(easeStart, easeEnd, startValue, endValue);
	}

	@Override
	protected double ease(final double positionRatio) {
		return this.startValue + (this.change * positionRatio);
	}
}
