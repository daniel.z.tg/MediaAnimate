package me.danielzgtg.mediaanimate.easing;

public final class QuarticInEasingNode extends EasingNode {
	public QuarticInEasingNode(final int easeStart, final int easeEnd, final double startValue, final double endValue) {
		super(easeStart, easeEnd, startValue, endValue);
	}

	@Override
	protected double ease(final double p) {
		return this.change * (p * p * p * p) + this.startValue;
	}
}
