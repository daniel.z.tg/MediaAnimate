#!/bin/bash

INSTALLDIR=$(dirname ${BASH_SOURCE[0]})
if [ -z "$INSTALLDIR" ]; then
    echo "Cannot determine MediaAnimate install directory"
elif [ "$0" != "/bin/bash" ]; then
    echo "Please use /bin/bash and run like: source path/to/mediaenv.sh"
else
    echo "MediaAnimate is at "$INSTALLDIR
    pushd $INSTALLDIR > /dev/null

    MEDIAJAR=$(readlink -f ./build/libs/MediaAnimate.jar)
    if [ ! -f $MEDIAJAR ]; then
        echo "MediaAnimate.jar not found"
    else
        alias mediabuild="java -jar $MEDIAJAR"
        alias mediarebuild="unlink output.mp4; mediaclean; mediabuild && mediastitch && mediaclean"
        alias mediastitch="ffmpeg -framerate 30 -i output/frame%04d.png output.mp4"
        alias mediaclean="rm -rf output"
        alias mediaveryclean="rm -rf output; unlink output.mp4 &>/dev/null"
        echo "Environment set for MediaAnimate"
    fi

    popd > /dev/null
fi

